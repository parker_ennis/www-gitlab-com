---
layout: handbook-page-toc
title: Performance Assessment and Succession Planning
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Talent Assessment Options

There are many Talent Assessment Options.
Some of the ones considered were:
* 9 Box: [The performance/potential matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix)
* 9 Box: [Talent placement](/handbook/people-group/performance-assessments-and-succession-planning/#talent-placement)
* 4 Box: [Skill/will matrix](/handbook/people-group/performance-assessments-and-succession-planning/#skillwill-matrix)

#### The Performance/Potential Matrix

9 Box Performance Potential matrix | | | |
|-----------------|---------------|---------------|----------------|
| **High Performance** | Good current performance but with risk of inability to grow leadership capabilities, expertise and future focus as required by the business; focus is on developing leadership capability with emphasis on | Strong business performance and strong day to day management of the business/function; management style is focused on today’s challenges; focus is on developing leadership capability with emphasis on innovation | Exceptional performance coupled with strong leadership capability to drive innovative growth of business; focus is on leveraging against critical needs of business |
| **Performance at expectations** | Solid business performance today but with risk of falling behind due to lack of leadership capability | Solid management and performance for today’s business challenges; potential to maximize contribution in current business model; focus on development of leadership capabilities and maintaining required expertise | Solid Performance with high potential to make significant contributions to the business; positioned to excel by leveraging leadership capabilities in challenging assignments |
| **Underperformance** | Below average performance and inconsistent leadership behaviors; operating from previous business models despite feedback focus is on performance management | Average to below average performance with limited capacity to increase performance through management and leadership capabilities; focus is on evaluating fit in the role; development is on improving performance | High Potential leader who is underperforming due to newness of role, business strategy or management team: focus is on enhancing contribution and leveraging leadership capabilities |
| Performance ↑ Potential → <b> | **Low Potential** | **Medium potential**| **High Potential**|

This matrix is an individual assessment tool that evaluates both an employee's current contribution to the organization and their potential level of contribution.
It is commonly used in succession planning as a method of evaluation an organization's talent pool, identifying potential leaders, and identifying any gaps or risks.
It is regularly considered a catalyst for robust dialogue and is considered more accurate than one person's opinion.
The performance/potential matrix can be a diagnostic tool for development.

##### What is "Potential"?

Potential here refers to the ability for a team member to move to the next level and scale with the company compared to peers.

#### Talent Placement

The Talent placement process is loosely as follows:
* Managers complete a 9 box performance/potential matrix for their staff; Roll up through Executive Team
* Talent Review Manager Meeting: Alll people managers come together to review and discuss results across the organization for each role. Changes are made as appropriate.
* Executive Review: Leadership meets to review the TRP Results; Promotion Requests; Review Development and Performance Actions; Review Discrepancies; Monitor Progress from previous TRP series.
* Final TRP results shared with managers: Manager education session with next steps and action plans

From beginning to end this process is typically 4-6 weeks.

| Competencies → <br> Accountabilities ↓ | Less effective <br> | Effective <br> | Most Effective <br> |
|----------------------------------------|----------------|-----------|----------------|
| Most effective                         | 5              | 2         | 1              |
| Effective                              | 7              | 4         | 3              |
| Less effective                         | 9              | 8         | 6              |

Boxes 1, 2, and 3 consistute your high performers, which are usually about 25% of the company.
Boxes 7, 8, and 9 will require focus to improve and sustain performance.
These are usually 10-15% of the organization.
Roughly 60-65% of the organization will fall into Boxes 4, 5, and 6.
These are your Core Performers.
All distributions are suggested by level (i.e. you wouldn't compare an Intermediate IC with a Director).

Employees in Boxes 1, 2, 3, and 4  are those who consistently demonstrate the highest potential for future success and promotability.

#### Skill/will matrix

The Skill/will matrix is used to determine what is the best management approach for a given person, based on their level of skill and their level of will.
This is a snapshot, not a holistic assessment.
A person is rarely in one quadrant all of the time.
This is a guide, not a mandate.

This matrix is best used when assessing a team member for a specific task or role, where skill is how competent or able (i.e. experience, training, knowledge, talent) a person is to do something, and will is how motivated or eager (i.e. desire to achieve, initiative, security surrounding task/job, confidence in abilities, feelings about the task) they are to do something.

| Skill → <br> Will ↓  | Low <br> | High <br> |
|----------------------|-------------|--------------------|
| <strong>Low</strong> | Manage Out  | Train & Develop    |
| <strong>High</strong> | Action Plan | Delegate & Promote |


## Succession Planning

The succession planning process starts with leaders doing a [performance/potential matrix](/handbook/people-group/performance-assessments-and-succession-planning/#the-performancepotential-matrix) of each of their direct reports.
This should especially highlight the movement of direct reports quarter of quarter in any direction.

The resulting charts are reviewed with peers, e.g. all other executives, all other senior leaders in the function, etc.
Then 1 succession planning table

| Person    | Jane Doe | John Doe |
|-----------|---|---|
| Role      | Job Title  | Job Title  |
| Emergency | Someone who could take over this role if the current person were affected by a [lottery factor](/handbook/total-rewards/compensation/#competitive-rate) or had to take emergency leave |   |
| Ready Now | Someone who could be promoted into the role today  |   |
| 1-2 Years | Someone who could be trained and elevated into the role in 1-2 years  |   |
| 3-5 Years | Someone who could be trained and elevated into the role in 3-5 years  |   |

## Developing future leaders in other layers of the org chart

When completing this exercise, it is important to present up and coming, high performers in the org who are not direct reports to the executive group (or the level completing the exercise). 
They can be someone at any level of the org chart.
Leaders should also feel comfortable highlighting team members in other functions who they identify as high performing. 
These may be people who are "up next" for promotions but also may not be.
This can help the leader's peers provide feedback on the developing leaders across GitLab.
