---
layout: markdown_page
title: "Category Direction - Accessibility Testing"
---

- TOC
{:toc}

## Accessibility Testing

Beyond being a compliance requirement in many cases, [accessibility testing](https://about.gitlab.com/blog/2020/03/04/introducing-accessibility-testing-in-gitlab/) is the right thing to do for your users. Accessibility testing is similar to UAT or Usability (which we track [here](/direction/verify/usability_testing/)), but we see accessibility as a primary concern on its own. Our vision for this category is to provide actionable data that can improve the accessibility of your web application within GitLab.

Interested in joining the conversation for this category? Please join us in the issues where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

- [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/3491)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAccessibility%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next for Accessibility Testing will be a [full view of the Accessibility report](https://gitlab.com/gitlab-org/gitlab/-/issues/36170) for a project. This is important for our Developer and Product Designer personas who want to understand outside of the context of the Merge Request what accessibility issues exist in a project.

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Track accessiblity results over time](https://gitlab.com/gitlab-org/gitlab/-/issues/36171)
- [Show a full accessibility report in GitLab](https://gitlab.com/gitlab-org/gitlab/issues/36170)
- [Add more accessibility scanners](https://gitlab.com/gitlab-org/gitlab/-/issues/218551)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/3491).

## Competitive Landscape

Competitors in this space are not providing first-party accessibility testing platforms, but do integrate with pa11y, lighthouse or other tools to generate results via the CI/CD pipeline.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category yet.

## Top Customer Issue(s)

With the release of GitLab 12.8 the top customer item ([gitlab#25566](https://gitlab.com/gitlab-org/gitlab/issues/25566)) was addressed. We look forward to feedback on the issues being tracked in the current epic for the category and beyond from customers.

## Top Internal Customer Issue(s)

The Design team has an open accessibility epic that contains items about making GitLab itself more accessible: [gitlab-org#567](https://gitlab.com/groups/gitlab-org/-/epics/567). The long term vision for this category is for that team to able to use GitLab to detect all of those issues in an automated way and see that they are addressed when fixed.

## Top Vision Item(s)

To meet our long term vision we will need to solve problems that will expand our out-of-the-box accessibility testing like helping customers track how [accessibility is changing over time](https://gitlab.com/gitlab-org/gitlab/issues/36171) and [providing additional scanners](https://gitlab.com/gitlab-org/gitlab/-/issues/218551) so users can be confident their web app is accessible by everyone.
