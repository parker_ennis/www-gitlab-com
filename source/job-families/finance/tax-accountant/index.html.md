---
layout: job_family_page
title: "Accountant, Tax Department"
---

## Job Grade
Accountant is a grade 6

## Responsibilities
1. Preparation and review of month management accounts
1. Contribute to the month-end close process
1. Contribute to filing statutory returns (including GST, VAT, Intrastat, local reports)
1. Contribute to the preparation and completion of CIT returns for GitLab’s entities
1. Contribute to the audit processes of annual financial statements
1. Contribute to the preparation of transfer pricing calculations
1. Ownership of filing deadlines for all our entities
1. Collaborate with local service providers and tax authorities on compliance requirements
1. Contribute to efficient, accurate and production of consolidated financial statements
1. Increase efficiency for existing processes and design new processes (e.g. use of AI)
1. Contribute to ERP improvements for tax reporting in Netsuite (VAT, Accounting)
1. Contribute to GitLab’s international expansion activities (e.g. entity setup)
1. Collaborate with the finance team in day-to-day activities, requests and projects
1. Contribute to the overall finance & tax OKR’s
1. Prepare meetings for the board of directors of GitLab BV
1. This position reports to the Director of Tax

## Requirements
1. Experience in public accounting
1. Experience with SEC companies
1. Software company experience
1. Knowledge of implementation of software solutions 
1. Experience with international subsidiaries
1. Tax experience preferred
1. IPO experience preferred
1. Knowledge of GAAP principles
1. Experience with Netsuite or comparable ERP system
1. Embrace the use of software solutions (e.g. Avalara, CrossBorder, VATit)
1. Proficient with Microsoft Office, G-Docs, G-Sheets
1. Flexible to meet changing priorities
1. Ability to prioritize workload to achieve on time accurate results
1. Detail-oriented, self-directed and able to effectively complete tasks
1. You share our values, and work in accordance with those values
1. Successful completion of a background check

## Requirements
1. 4 years of experience as VP Information Technology, CIO or similar role.
1. Outstanding knowledge of modern tech stack of cloud applications.
1. Extensive hands on experience with design, development and implementation of IT systems.
1. Solid understanding of data analysis and data engineering in a hyper growth company.
1. Demonstrated knowledge of budgeting, financial planning and business operations.
1. Analytical mind and great problem-solving skills.
1. Have or had extensive experience with IT helpdesk function including hardware deployment and system access.
1. Outstanding communication and interpersonal abilities and history of success working with executives across an organization.
1. BSc/BA in computer science or relevant field. MSc/MA is a big plus.
1. Ability to use GitLab

## Performance Indicators
1. [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
1. [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
1. [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below.
1. 30 minute screening call with recruiter
1. 45 minute interview with Director of Tax
1. 30 minute interview with Controller
1. 30 minute interview with Sr. External Accounting & Reporting Manager
1. 30 minute interview wirh Principle Accounting Officer

Please note that a candidate may declined from the position at any stage of the process.
Additional details about our process can be found on our [hiring page](/handbook/hiring).
