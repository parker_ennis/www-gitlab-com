# frozen_string_literal: true

require 'middleman'

#
# This middleman extension is used to speed up builds on CI
#
# We effectively do this by leveraging our CI parallelism features and split the build into nine
# distinct partitions:
#
# 1. BLOG_ONE = "blog posts old (up to 2015)"
# 2. BLOG_TWO = "2016 blog posts"
# 3. BLOG_THREE = "2017 blog posts"
# 4. BLOG_FOUR = "2018 blog posts"
# 5. BLOG_FIVE = "2019 & 2020 blog posts"
# 6. BLOG_SIX = "Release posts"
# 7. IMAGES = "IMAGES"
# 8. ASSETS = "javascript, stylesheets, icons, pdfs"
# 9. ALL_OTHERS = "all other pages"
#
# If `CI_NODE_INDEX` and `CI_NODE_TOTAL` are not set, e.g. on development machines, or turning
# parallelism off, it will simply be a noop extension
class PartialBuildBlog < Middleman::Extension
  BLOG_ONE = "blog posts old (up to 2015)"
  BLOG_TWO = "2016 blog posts"
  BLOG_THREE = "2017 blog posts"
  BLOG_FOUR = "2018 blog posts"
  BLOG_FIVE = "2019 blog posts"
  BLOG_SIX = "2020 and beyond blog posts"
  IMAGES = "IMAGES"
  ASSETS = "javascript, stylesheets, icons, pdfs"
  ALL_OTHERS = "all other pages"

  # We must ensure that this extension runs last, so that
  # the filtering works correctly
  self.resource_list_manipulator_priority = 1000

  def_delegator :@app, :logger

  def initialize(app, options_hash = {}, &block)
    super
    @enabled = ENV['CI_NODE_INDEX'] && ENV['CI_NODE_TOTAL'] || ENV['CI_BUILD_PROXY_RESOURCE']

    return unless @enabled

    raise "#{self.class.name}: If you want to enable parallel builds, please use exactly 9 parallel jobs" unless ENV['CI_NODE_TOTAL'].to_i == 9

    @partial = case ENV['CI_NODE_INDEX']
               when "1"
                 BLOG_ONE
               when "2"
                 BLOG_TWO
               when "3"
                 BLOG_THREE
               when "4"
                 BLOG_FOUR
               when "5"
                 BLOG_FIVE
               when "6"
                 BLOG_SIX
               when "7"
                 IMAGES
               when "8"
                 ASSETS
               when "9"
                 ALL_OTHERS
               else
                 raise "#{self.class.name}: Invalid Build Partial #{ENV['CI_NODE_INDEX']}. At the moment we only support 1 to 9"
               end
  end

  def blog_one?(resource)
    resource.destination_path.start_with?(
      'blog/2011',
      'blog/2012',
      'blog/2012',
      'blog/2013',
      'blog/2014',
      'blog/2015',
      'releases/2011',
      'releases/2012',
      'releases/2012',
      'releases/2013',
      'releases/2014',
      'releases/2015'
    )
  end

  def blog_two?(resource)
    resource.destination_path.start_with?(
      'blog/2016',
      'releases/2016'
    )
  end

  def blog_three?(resource)
    resource.destination_path.start_with?(
      'blog/2017',
      'releases/2017'
    )
  end

  def blog_four?(resource)
    resource.destination_path.start_with?(
      'blog/2018',
      'releases/2018'
    )
  end

  def blog_five?(resource)
    resource.destination_path.start_with?(
      'blog/2019',
      'releases/2019'
    )
  end

  def blog_six?(resource)
    resource.destination_path.start_with?(
      'blog/202',
      'releases/202'
    )
  end

  def images?(resource)
    resource.destination_path.start_with?('images/') && !assets?(resource)
  end

  def assets?(resource)
    resource.destination_path.start_with?(
      'ico/',
      'stylesheets/',
      'javascripts/',
      'devops-tools/pdfs/',
      'pdfs',
      'blog/blogimages'
    )
  end

  def all_others?(resource)
    !blog_one?(resource) &&
      !blog_two?(resource) &&
      !blog_three?(resource) &&
      !blog_four?(resource) &&
      !blog_five?(resource) &&
      !blog_six?(resource) &&
      !images?(resource) &&
      !assets?(resource)
  end

  def part_of_partial?(resource)
    return true if resource.destination_path == 'sitemap.xml'

    case @partial
    when BLOG_ONE
      blog_one?(resource)
    when BLOG_TWO
      blog_two?(resource)
    when BLOG_THREE
      blog_three?(resource)
    when BLOG_FOUR
      blog_four?(resource)
    when BLOG_FIVE
      blog_five?(resource)
    when BLOG_SIX
      blog_six?(resource)
    when IMAGES
      images?(resource)
    when ASSETS
      assets?(resource)
    when ALL_OTHERS
      all_others?(resource)
    else
      raise "#{self.class.name}: You are trying to build a unknown partial: #{@partial}"
    end
  end

  def manipulate_resource_list(resources)
    unless @enabled
      logger.info "#{self.class.name}: CI environment variables were not set for a partial build; building everything"
      return resources
    end

    logger.info "#{self.class.name}: We are building the partial: #{@partial}"

    resources.select { |resource| part_of_partial?(resource) }
  end
end

::Middleman::Extensions.register(:partial_build_blog, PartialBuildBlog)
