---
layout: handbook-page-toc
title: "UX Research"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

## UX research at GitLab

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals when using GitLab. We use these insights to inform and strengthen product and design decisions.

UX Researchers aren't the only [GitLab Team Members](https://about.gitlab.com/handbook/communication/#top-misused-terms) who conduct user research. Other roles, like Product Managers and Product Designers, frequently conduct research too with guidance from the UX research team.

### Quick links to UX research resources

- [UX Research project](https://gitlab.com/gitlab-org/ux-research) 
- [UX research coordination at GitLab](/handbook/engineering/ux/ux-research-coordination/)
- [Training resources](/handbook/engineering/ux/ux-research-training/)
- [Qualtrics tips & tricks](/handbook/engineering/ux/qualtrics/)
- [Dovetail tips & tricks](/handbook/engineering/ux/dovetail/)
- [Shared Google Drive for research videos and artifacts](https://drive.google.com/drive/folders/0AH_zdtW5aioNUk9PVA) - Internal access only.
- [UXR Insights project](https://gitlab.com/gitlab-org/uxr_insights/) - No longer maintained or in use.

### Research methods, workshops, and exercises

Research is successful when we understand our research goals and why we choose one method over another. Knowing and understanding the question(s) you want to learn about will help you determine which method to use.

We use a wide variety of research methods, workshops, and exercises to help us better understand our users’ needs and goals. Our teams are encouraged to seek out anything that will allow our teams to better collaborate while we continue to enhance our product experience. Some of these are listed below:

#### Problem Validation
##### Research Methods
<details>
<summary markdown="span">User Interviews</summary>

**Resources**

* [Interviewing Users](https://www.nngroup.com/articles/interviewing-users/) (NNGroup)
* [Why User Interviews Fail](https://www.nngroup.com/articles/why-user-interviews-fail/?lm=interviewing-users&pt=article) (NNGroup)
* [User Interviews: How, When and Why to Conduct Them](https://www.nngroup.com/articles/user-interviews/?lm=interviewing-users&pt=article) (NNGroup)
* [Individual Interviews](https://www.usability.gov/how-to-and-tools/methods/individual-interviews.html) (Usability.gov)
* [User Interviews at GitLab](#for-user-interviews)

</details>

<details>
<summary markdown="span">The Five Whys</summary>

**Resources**

* [Design Kit: The 5 Whys](https://www.designkit.org/methods/66) (IDEO)

</details>

<details>
<summary markdown="span">Surveys</summary>

**Resources**

* [Online Surveys](https://www.usability.gov/how-to-and-tools/methods/online-surveys.html) (Usability.gov)
* [Surveys at GitLab](#for-surveys)

</details>

##### Workshops
<details>
<summary markdown="span" id="remote-desgin-sprint">Remote Design Sprint</summary>

**Resources**

* [How To Run Amazing Remote Design Sprints](https://www.youtube.com/watch?v=IFHfsRNTGCM) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">Lightning Decision Jam</summary>

**Resources**

* [Desig Sprint Workshop-LDJ](https://www.youtube.com/watch?v=33hBnZzoFAg) (AJ&Smart - video)

</details>

##### Exercises
<details>
<summary markdown="span">Expert Interview</summary>

**Resources**

* [Ask The Experts](https://www.youtube.com/watch?v=ZtYp7XzmXr8&t=491s)(AJ&Smart - video)

</details>

<details>
<summary markdown="span">"How Might We"</summary>

**Resources**

* [A Design Sprint Exercise for Problem Solving](https://spin.atomicobject.com/2018/12/12/how-might-we-design-thinking/)(Atomic Object)
* [Turn Your Perspective Into Actionable Provocations](https://static1.squarespace.com/static/57c6b79629687fde090a0fdd/t/589cc8b8d2b85721b37d3efe/1486670008488/HMW-Worksheet.pdf) (DSchool - PDF)
* [How Might We...?](http://crowdresearch.stanford.edu/w/img_auth.php/f/ff/How_might_we.pdf) (DSchool)

</details>

<details>
<summary markdown="span">Long-Term Goal</summary>

**Resources**

* [Long-Term Goal & Sprint Questions](https://www.youtube.com/watch?v=OaeKpGJe2To&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=4) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">The Map</summary>

**Resources**

* [Map](https://www.youtube.com/watch?v=JTqCR84fzeg&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=5) (AJ&Smart - video)
* [How To Draw The Map](https://www.youtube.com/watch?v=yYNpOKq3Wfg&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=14) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">Lightning Demos</summary>

**Resources**

* [Lightning Demo](https://www.youtube.com/watch?v=1iH9jkJHbs4&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=6) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">Buy a Feature</summary>

**Resources**

* [Buy a Feature](https://www.innovationgames.com/buy-a-feature/) (Inovation Games)

</details>

<details>
<summary markdown="span">Competitive Analysis</summary>

**Resources**

* [Competitive Analysiss](https://medium.com/user-research/competitive-analysis-b02daf26a96e) (Medium)

</details>

#### Design
##### Research Methods
N/A

##### Workshops
<details>
<summary markdown="span">4 Part Sketching</summary>

* Note Taking
* Doodling
* Crazy 8s
* Solution Concept

**Resources**

* [4-Step Sketching](https://www.youtube.com/watch?v=TK-94QiEFgw&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=7) (AJ&Smart - video)

</details>

##### Exercises
<details>
<summary markdown="span">User Test Flow</summary>

**Resources**

* [Storyboad Hack - User Test Flow](https://www.youtube.com/watch?v=y5pSBgQUezQ&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=9) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">Storyboarding</summary>

**Resources**

* [Design Kit: Storyboard](https://www.designkit.org/methods/35) (IDEO)
* [Storyboards Help Visualize Ideas](https://www.nngroup.com/articles/storyboards-visualize-ideas/) (NNGroup)
* [Storyboard Template](https://media.nngroup.com/media/editor/2018/06/20/storyboard-template.pdf) (NNGroup - PDF)
* [How To Draw The Storyboard](https://www.youtube.com/watch?v=yav4HN26XT8&list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j&index=10) (AJ&Smart - video)

</details>

<details>
<summary markdown="span">Wireflows</summary>

**Resources**

* [Wireflows](https://www.nngroup.com/articles/wireflows/) (NNGroup)

</details>

#### Solution Validation
##### Research Methods
<details>
<summary markdown="span">Card Sorts</summary>

**Resources**

* [Card sorting](https://www.usability.gov/how-to-and-tools/methods/card-sorting.html) (Usability.gov)
* [Card Sorting 101](https://www.optimalworkshop.com/learn/101s/card-sorting/) (Optimal Workshop)

</details>

<details>
<summary markdown="span">Tree Tests</summary>

**Resources**

* [Tree tests](https://en.wikipedia.org/wiki/Tree_testing) (Wikipedia)
* [Tree Testing 101](https://www.optimalworkshop.com/learn/101s/tree-testing/) (Optimal Workshop)

</details>

<details>
<summary markdown="span">First Click Tests</summary>

**Resources**

* [First-Click Testing](https://www.usability.gov/how-to-and-tools/methods/first-click-testing.html) (Usbility.gov)
* [First-Click Testing 101](https://www.optimalworkshop.com/learn/101s/first-click-testing/) (Optimal Workshop)
* [An Introduction to First-Click Testing](https://usabilityhub.com/guides/first-click-testing) (UsabilityHub)

</details>

<details>
<summary markdown="span">Preference Tests</summary>

**Resources**

* [An Introduction to Preference Tests](https://usabilityhub.com/guides/preference-testing) (UsabilityHub)

</details>

<details>
<summary markdown="span">Five Second Tests</summary>

**Resources**

* [An Introcution to Five-Second Testing](https://usabilityhub.com/guides/five-second-testing) (UsabilityHub)

</details>

<details>
<summary markdown="span">Beta Testing</summary>

**Resources**

* [Beta Testing](https://www.intercom.com/blog/how-to-run-a-successful-beta) (Intercom Blog)

</details>

<details>
<summary markdown="span">Moderated Usability Testing</summary>

**Resources**

* [Checklist For Planning Usability Studies](https://www.nngroup.com/articles/usability-test-checklist/) (NNGroup)
* [Usability Testing](https://www.usability.gov/how-to-and-tools/methods/usability-testing.html) (Usability.gov)

</details>

<details>
<summary markdown="span">Unmoderated Usability Testing</summary>

**Resources**

* [Moderated vs. Unmoderated Usability Testing: The Pros and Cons](https://www.usertesting.com/blog/moderated-vs-unmoderated-usability-testing) (UserTesting.com)
* [How to Run an Unmoderated Remote Usability Test](https://uxmastery.com/how-to-run-an-unmoderated-remote-usability-test-urut/) (UX Mastery)

</details>

<details>
<summary markdown="span">5-Act Walkthrough</summary>

**Resources**

* [5-Act Interview](https://www.youtube.com/watch?v=U9ZG19XTbd4&t=0s) (Jake Knapp)

</details> 

<details>
<summary markdown="span">UX Scorecard</summary>

**Resources**

* [UX Scorecards](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/) (GitLab)

</details>

<details>
<summary markdown="span">Surveys</summary>

**Resources**

* [Surveys](https://www.usability.gov/how-to-and-tools/methods/online-surveys.html) (Usability.gov)

</details>

##### Workshops
* [Remote Design Sprint](#remote-design-sprint)

##### Exercises
N/A

### Research tools

Product Managers, Product Designers and UX Researchers have access to the following tools:

[Qualtrics: CoreXM with TextiQ](https://www.qualtrics.com/uk/core-xm/) - Used for surveys, screening surveys and contacting members of GitLab First Look. To request access to Qualtrics, [please open an access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and assign the issue to your manager for approval. Once approved, please assign the issue to `@sarahj`. For further information about Qualtrics, including how to create and style your survey, please visit [Qualtrics tips & tricks](/handbook/engineering/ux/qualtrics/).

[Mural](https://mural.co/) - For online brainstorming, synthesis and collaboration. Please reach out to a UX Design Manager, Christie Lenneville or Sarah Jones for access.

[Calendly](https://calendly.com/) - For scheduling research sessions with users. A basic plan (free) is usually adaquate for Product Managers and Designers. UX Researchers are entitled to a Premium account. Should you wish to upgrade your Calendly account to Premium, please contact `@sarahj`.

[Dovetail](https://dovetailapp.com/) - We use Dovetail to analyze data, collaborate on insights and as a research repository.

[Zoom Pro Account](https://zoom.us/) - We use Zoom to run usability testing sessions and user interviews. All new team members at GitLab automatically receive a Zoom Pro account.

[Optimal Workshop](https://www.optimalworkshop.com) - Used for [card sorts](#card-sorts) and [tree testing](#tree-tests). We do not have an ongoing subscription to Optimal Workshop. We purchase a monthly license as and when required.  

[Maze](https://maze.design/) - Used for unmoderated, quick-hit usability testing where you can provide a click-through prototype with a survey follow up. We do not have an ongoing subscription to Maze. Maze does offer a free plan that allows you to run one project at a time.

[UsabilityHub](https://usabilityhub.com/) - Used for [First-Click tests](#first-click-tests), and [Preference tests](#preference-tests), [Five Second tests](#five-second-tests). We do not have an ongoing subscription to UsabilityHub. Work with your manager if a license is required.

### How to find existing research

- [Dovetail](https://dovetailapp.com/) is the single source of truth (SSOT) for all user insights discovered by GitLab’s UX Researchers, Product Designers, and Product Managers. 

- [UXR_Insights project](https://gitlab.com/gitlab-org/uxr_insights) - Prior to Dovetail, the UX Research team used GitLab as a research repository. Instead of reports and slide decks, we used issues to document key findings from research studies. A directory of completed research is available in the project's [ReadMe](https://gitlab.com/gitlab-org/uxr_insights/blob/master/README.md) file. The UXR_Insights project is no longer maintained or in use.

- [GitLab Community Forum](https://forum.gitlab.com) - The forum is ran by community members. It's a place for community members to share, ask and discuss everything related to GitLab.

- [Zendesk](https://www.zendesk.com) - Zendesk is a ticketing system used by GitLab's Support team to track problems raised by customers. [Zendesk accounts](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) are available for all GitLab staff.

- [Chorus.ai](https://www.chorus.ai) - Chorus.ai is used by our Sales team to record conversations that they have with GitLab customers. It has the ability to transcribe and highlight key points during these conversations. To request access to Chorus.ai, [please open an access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

- [GitLab issue tracker](https://gitlab.com/gitlab-org/gitlab/issues)

- Social media and public forums - Such as [Twitter](https://www.twitter.com), [HackerNews](https://news.ycombinator.com/) and [Reddit](https://www.reddit.com/r/gitlab/).  

### How we decide what to research

UX Researchers collaborate with Product Managers to determine the scope of research studies. Where possible, UX Researchers should try to attend planning meetings for their designated groups. 

UX Researchers should proactively offer ways in which they can assist in the delivery of research. They should also suggest and discuss their own ideas for research studies with Product Managers.

### How UX Researchers operate at GitLab

1. Assign yourself to any issues you are leading or supporting.

1. Re-organize your issues in the [UX Research board](https://gitlab.com/groups/gitlab-org/-/boards/1540253) as appropriate
	- `Doing` = issues you are working on right now.
	- `To Do` = issues you plan to tackle in the near future.
	- `Blocked`= issues you can't proceed with as you're waiting on feedback from someone else.
	- `Backlog` = issues that haven't yet been scheduled.
	- Try to order by priority (top down).
	- Aim to update the board at least once a week.

1. Add a weight to every `Problem validation research` issue.
	- `1` = Trivial
	- `2` = Small
	- `3` = Medium
	- `5`= Large
	- `8` = Huge
	- `13` = Gigantic

1. Proactively contact your Product Director, and ask them to prioritize your issues. Inform your Product Director of your capacity based on your historic velocity. This may be more effective as a Zoom call at first, rather than async.

1. Move any issues which are not prioritized into your `Backlog`. 

### How UX Research and Product Management work together on problem validation research (single-stage-group initiatives)

1. Follow the [initial steps](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation) outlined in the Product handbook.

#### For user interviews

1. Product Manager drafts the [discussion guide](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/discussion-guide-user-interviews/). When a first draft of the guide is complete, the UX Researcher reviews and provides feedback.

1. Simultaneously, the UX Researcher begins crafting a screening survey in Qualtrics.

1. After the screening survey is created, the UX Researcher will open a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assign it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The Research Cordinator will perform a sense check to make sure your screener will catch the people you’ve identified as your target participants. If there are multiple rounds of review, the Coordinator will pause activities until uncertainty about your screening criteria has been resolved.

1. By default, Product Managers are responsible for leading (moderating) problem validation interviews with users. If the study is complex in nature, a UX Researcher may volunteer to moderate. UX Researchers have the discretion to decide which research studies they will moderate and which they will observe synchronously or asynchronously.

1. The person who is leading the interviews, and who has subsequently supplied their Calendly link to the Research Coordinator, is responsible for forwarding user interview invites to the UX Research calendar (`gitlab.com_kieqv96j35mpt8bdkcbriu2qbg@group.calendar.google.com`) and any other interested parties (Product Designers, Product Managers, UX Researchers, etc).

1. After the interviews are concluded, the UX Researcher updates the `Recruiting request`. The Research Coordinator will reimburse participants for their time (payment occurs on Tuesdays and Thursdays).

1. Product Manager and UX Researcher work collaboratively to synthesize the data and identify trends in Dovetail, resulting in insights.

1. UX Researcher updates the `Problem validation research` issue with links to findings in Dovetail and, if applicable, unmarks the `Problem validation research` issue as `confidential` before closing it.

#### For surveys

1. Product Manager drafts the survey. When a first draft of the survey is complete, the UX Researcher reviews and provides feedback.

1. The UX Researcher enters the survey in Qualtrics.

1. Once the survey has been entered into Qualtrics, the UX Researcher opens a `Recruiting request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/) and assigns it to the relevant [Research Coordinator](https://about.gitlab.com/company/team/?department=ux-research-team).

1. The Research Coordinator distributes the survey to a sample of participants.

1. The UX Researcher, will review the responses received so far and amend the survey if necessary. The UX Researcher should advise the Research Coordinator when to continue recruitment.

1. The UX Researcher will keep the Research Coordinator informed of the survey's response rate and must notify them when they plan to close the survey (to ensure recruitment doesn't continue on a survey that has been closed).

1. After the survey is closed, the UX Researcher updates the `Recruiting request` issue. The Research Coordinator will reimburse selected participants for their time (payment occurs on Tuesdays and Thursdays).

1. Product Manager and UX Researcher work collaboratively to synthesize the data and identify trends, resulting in insights.

1. UX Researcher documents insights in Dovetail.

1. UX Researcher updates the `Problem validation research` issue with links to findings in Dovetail and, if applicable, unmarks the `Problem validation research` issue as `confidential` before closing it.


### How UX Researchers work together on problem validation research (multi-stage-group initiatives)

Note: A Lead UX Researcher is the person who devises the research brief and provides an initial outline of the study's goals and hypotheses. A Lead UX Researcher can hold any level of seniority and experience, from Research Coordinator to Staff UX Researcher. The Lead UX Researcher is the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) for the research study. They should communicate that they are the DRI during stakeholder meetings and within research issues.

1. Lead UX Researcher meets with the relevant PM(s), fellow UX Researcher(s), Research Coordinator(s) and UX Research Manager to discuss the goals of the study and the hypotheses they have. The purpose of this meeting is to get buy-in from all stakeholders. If a stakeholder is unable to attend the meeting, record the session, and allow them the opportunity to provide their feedback asynchronously.

1. Lead UX Researcher creates an issue using the [Problem Validation template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/issue_templates/Problem_Validation.md).

1. Lead UX Researcher applies the `~"workflow::problem validation"` label to the associated issue; this automatically removes the `~"workflow::validation backlog"` label.

1. Lead UX Researcher fills out an [opportunity canvas](#opportunity-canvas) to the best of their ability. Ensure the problem and persona is well articulated and add the opportunity canvas to the issue's [Designs](https://docs.gitlab.com/ee/user/project/issues/design_management.html#the-design-management-page). Note that you should include content for the solution and go-to-market sections, possibly with low confidence; this section may be likely to change, but thinking it through will help clarify your thoughts. 

1. Lead UX Researcher opens a `Problem validation research` issue using the available template in the UX Research project. Once completed, assigns the issue to the relevant UX Researcher(s).

1. Follow the steps outlined for [user interviews](#for-user-interviews) or [surveys](#for-surveys). However, for multi-stage-group initiatives, the Lead UX Researcher should take responsibility for all steps outlined in the user interview or survey process, for example, writing a discussion guide. They are welcome to delegate responsibilities to other UX Researchers should they need assistance.

1. Lead UX Researcher meets with the relevant PM(s), UX Researcher(s) and UX Research Manager to discuss findings and next steps. 
	
	The Lead UX Researcher may be required to:

	1. Finalize the opportunity canvas with the synthesized feedback.

	1. Present the opportunity canvas to Scott Williamson, Christie Lenneville, and the relevant Product Director(s).

### Solution validation

[Solution validation research](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-4-solution-validation) at GitLab is led by Product Designers, with support from UX Design Managers. Occassionally, UX Design Managers may need to escalate queries about solution validation research to UX Researchers for advice and feedback. UX Researchers **are not** responsible for leading solution validation research efforts.

### How to request research

Any [GitLab Team Member](https://about.gitlab.com/handbook/communication/#top-misused-terms) can open a research request. If you are **not** a Product Manager, Product Designer, or UX Researcher, please open a `Research request` issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [Product Manager](https://about.gitlab.com/handbook/product/categories/), [Product Designer](https://about.gitlab.com/handbook/product/categories/) and [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team). The team will review your issue and notify you when/if they plan to proceed with the work.

### Milestones

Like other departments at GitLab, UX Researchers follow the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) and use milestones to schedule their work. Milestones change monthly, [find out the dates for upcoming milestones](https://gitlab.com/groups/gitlab-org/-/milestones).

### Training resources

If you're new to conducting user research or just want a refresher, please refer to the [UX research training resources](/handbook/engineering/ux/ux-research-training/) to help you get started.

Product Designers and Product Managers should complete the [Research Shadowing](/handbook/engineering/ux/ux-research-training/research-shadowing/) process during their onboarding to gain an understanding of how research is conducted at GitLab.

### UX Research label
Both the [GitLab CE project](https://gitlab.com/gitlab-org/gitlab-ce) and [GitLab EE project](https://gitlab.com/gitlab-org/gitlab-ee) contain a `UX Research` label. The purpose of this label is to help Product Designers and Product Managers keep track of issues which they feel may need UX Research support in the future or which are currently undergoing UX Research. 

UX Researchers are not responsible for maintaining the `UX Research` label. The `UX Research` label **should not** be used to request research from UX Researchers. 
