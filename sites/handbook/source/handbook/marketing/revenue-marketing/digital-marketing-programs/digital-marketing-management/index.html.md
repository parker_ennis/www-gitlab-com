---
layout: handbook-page-toc
title: "Digital Marketing Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Managers
Digital Marketers are responsible for all inbound marketing.

## DMP labels in GitLab

*Note*: Some of the following labels only exist on the Digital Marketing Programs project level.

* **Digital Marketing Programs**: General label to track all issues related to Digital Marketing Programs
* **SEM**: Used for issues that require organic and paid search initiatives
* **Paid Ads**: Used for any paid advertising campaign such as Google Ads
* **Paid Social**: Used for paid social campaigns such as LinkedIn InMail and Facebook Ads 

## DMP Slack channels

*  `#digital-marketing`: General digital marketing conversation and questions
*  `#dmpteam`: Discussion for DMP team members

## Paid Digital Marketing 

### Goals and objectives accomplished with digital campaigns:
* Lead Generation
* Brand Awareness
* Event Registrations
* Webcast Views
* Content Downloads
* Website Traffic

### How does paid digital contribute to GitLab’s funnel?
* Top of Funnel: Introduce GitLab brand to potential customers with awareness and reach objectives.
* Middle of Funnel: Nurturing engaged prospects with educational messaging & content.
* Bottom of Funnel: Leading prospects to conversion by retargeting with relevant, personalized ad experiences.

### Digital Campaign Types
DMPs can recommend specific types based on your campaign goals. The most common type is Paid Social based on robust targeting criteria and successful performance in reach and lead volume.

#### Channels
* Paid Social
   - LinkedIn (InMail Messaging Ads and Sponsored Content Ads)
   - Facebook (Single Image & Video Ads, Lead Generation Ads, Carousel Ads)
   - Twitter (Promoted Tweets, Image or Video)
* Paid Search (Google and/or Bing)
* Paid Display (Google Display Network)
   - Please note that Demandbase is a digital marketing platform for Display, but focuses on named accounts only, so campaign requests should be created under the [Account Based Marketing project](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/tree/master).
* Content Syndication
* Sponsorships: 
   - Virtual Event (Virtual Conference, Panel, Talking Head, All-Day Event/Summit)
   - Custom Webcasts (single & multi-vendor)
   - Microsites
   - Newsletter ads
   - Custom email blasts
   - Sponsored Custom & 3rd Party Content Creation (trend reports, ebooks, articles, etc.)


### Program definitions

#### **Paid search**: 
Paid search are [text ads](https://support.google.com/google-ads/answer/1704389?hl=en) on Google and/or Bing search engines to drive people to specific GitLab landing pages as they are looking for information on search engines. We do this by [bidding](https://support.google.com/google-ads/answer/2459326?hl=en) on targeted keywords and phrases based on the assumed intent of the person and matching that intent with a related landing page. 
* **Best used for**: Mid and bottom funnel content where we want someone to take action (ex: filling our a form to a gated asset, signing up for a demo, etc).
     * **Best type of content to use**: Use case type gated assets that directly applies to the intent for the search query. How to guides and ebooks, do well here. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Caveat here is if the content is more of a how to report or guide. 

#### **Display ads**:
Display ads are banner ads that we mostly run are through the [Google Display Network](https://support.google.com/google-ads/answer/2404190?hl=en). Banner ads will show on websites that have [Google Adsense](https://support.google.com/adsense/answer/6242051?hl=en) set-up on their website. There are no specific websites we show banner ads on - we earn the ad space by bidding on placements based on specific targeting criteria such as demographics, topics, and interests. On occassion, we run banner ads on websites through direct buys. This is handled more in the publisher program. 
* **Best used for**: Top and mid funnel content. More used for awareness and some action based response.
     * **Best type of content to use**: Use case type gated assets. How-to guides and ebooks do well here. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Caveat here is if the content is more of a how to report or guide. 

* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Remarketing**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website. 

#### **Paid social**:
Paid social ads are ads that we show on social platforms. The three social media platforms that we primarily advertise on are Facebook/Instagram, LinkedIn (this includes LinkedIn InMail), and Twitter. 
* **Best used for**: Top and mid funnel content. Does well for both awareness and direct response (depending on the asset used).
     * **Best type of content to use**: Live webcasts, recorded webcasts, events, and ebooks/guides 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports.
* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Remarketing**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website.

#### **Publisher sponsorships**:
Publisher sponsorships are when we engage a specific publisher in order to purchase placement on their web properties. Generally, we make sure the publisher's website(s) and audience closely match the profile of who we want to advertise to before engaging with the publisher. Additionally, we want to make sure the programs that are offered by the publisher align with our goals. 
* **Best used for**: Primarily used for demand generation, so we focus on mid to bottom funnel content.
     * **Best type of content to use**: Live webcasts and recorded webcasts work the best. Ebooks and guides sometimes work, depending on the placement. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports.
     
### Digital Campaign Design Specs
Each paid channel has its own unique design specifications and recommendations for their ad types to ensure ads can run at their optimal performance. If you do not yet have creative assets secured for your campaign, the design team can use this section as their guide when producing your creative.

#### Paid Social
* Facebook Image:
   - Recommended Image Size: 1200x628 pixels
   - Recommended Image Ratio: 1.91:1 to 4:5
   - Recommended Image File Type: JPG or PNG
   - No buttons allowed
   - Do not include text in the creative
* Facebook Video:
   - Recommended Video Ratio: 9:16 (full vertical) to 16:9 (feed/landscape)
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 4GB Max
   - Recommended Video Length: 5-15 seconds
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
* LinkedIn Image:
   - Recommended Image Size: 1200x627 pixels
   - Recommended Image Ratio: 1.91:1
   - Recommended Image File Type: JPG or PNG
   - Recommended Image File Size: 5MB Max
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* LinkedIn Video: 
   - Recommended Video Length: Less than 15 seconds
   - Recommended Video File Size: 75 KB to 200 MB
   - Recommended Video File Format: MP4
* Twitter Image: 
   - Recommended Image Size: 1200x675 pixels
   - Recommended Image Ratio: 1:1
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* Twitter Video:
   - Recommended Video Size: 1200x1200
   - Recommended Video Ratio: 1:1
   - Recommended Video Length: less than 15 seconds
   - Recommended Branding: Consistent in upper left-hand corner
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 1GB Max

#### Paid Display (Google Display Network)
* Image Sizes
   - 160x600
   - 250x250
   - 300x1050
   - 300x250
   - 300x600
   - 320x50
   - 336x280
   - 728x90
   - 970x250
* Recommended Image File Type: JPG or PNG
* Recommended Image File Size: 150KB Max
* In-Banner Design High Performers
   - Benefit/Value Prop and educational messaging
   - CICD emblem background
   - “Learn More” CTA
   - 4-7 word volume

## Requesting Digital Marketing Promotions

If you would like to request a paid digital marketing promotion in paid search, paid social, paid sponsorships or other paid marketing to support your event, content marketing, or webcast, asset, etc. create an issue in the [Digital Marketing Programs Repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/tree/master) and the follow [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=paid-digital-request).

If you request a digital marketing promotion you probably also need a marketing campaign and should consult with the [Marketing Program Managers](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/).

### Issue Items Defined

* **Campaign Name**: Name of the campaign that you are requesting
* **Campaign Budget**: How much budget you would like to allot to this campaign
* **Campaign/Finance Tag**: If you have a specific Campaign/Finance Tag that you need us to bill to, please input here. These will also be used as the `utm_campaign` name in reporting (minus spaces, underscores, and special characters)
* **Team Making Promotion**: Team that we should bill to (i.e. Field Marketing - AMER, Corporate Marketing, etc). 
* **Campaign Description**: Brief description of the campaign and what you are trying to achieve
* **Campaign Start and End Dates**: Dates of when the campaign should start and end
* **Campaign Goal**: What is the numeric goal and KPI for the campaign (registrations, page views, downloads, etc)
* **Campaign Target Audience**: The type of people that you want to reach in this campaign
* **Campaign Creative Asset**: If you have creative set already for this campaign, please provide links to the creative. If not, this may require a separate ask for Design team, please make request in Corporate project and factor into timeline

### DMP Request Workflows

**1. Paid Ads  (Search, Social, Display)** 
* Before creating an issue in Digital Marketing Programs, this information is required:
   * Budget
   * Start and end dates
   * General targeting parameters
   * Prepared account list (if applicable)
   * Geo targeting within certain regions
   * Landing Page URL(s)
   * Campaign/Finance tag
   * If you would like guidance from the DMP around certain items, please make a comment within the issue.
* Assign the issue to:
   * Leslie Stinson for Field, Microcampaigns, ABM, and Acceleration.
   * Matt Nguyen for Integrated Campaigns, non-campaign webcasts, and Corporate initiatives (such as All Remote).
* DMP will then create an issue for our digital agency PMG.
   * Only DMP can create issues within Digital Advertising (our project for PMG).
   * DMP will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
   * Please do not comment in PMG issues within Digital Advertising unless you are added as a participant.
* DMP or MPM will create a design request issue if no creative is provided.
* DMP will approve copy & mockups.
   * PMG will generate copy & CTAs based on the landing page or content brief.
   * DMP will approve copy and mockups, unless approval from the campaign owner is requested.
* If testing is requested, DMP will secure testing variations (copy, CTA, and/or creative) and propose a schedule for testing rounds.
* * DMP will confirm launch within the issue.
* DMP will provide campaign summary and report after the campaign ends.

**2. Publisher Engagements (Content Syndication, Hosted Webinars, Lead Generation Packages, and Sponsorships)** 
* Before creating an issue in Digital Marketing Programs, this information is required:
   * Budget
   * Start and end dates
   * General targeting parameters (if applicable)
   * Landing Page URL(s)
   * Campaign/Finance tag
   * Along with Publisher description and contact information, please provide additional information for specific campaign type:
      * Sponsorships: ad placement information, including creative specs
      * Content Syndication: content to promote (PDF file recommended)
      * Lead Generation Packages: lead list information and upload & implementation strategy 
      * Hosted Webinar: webinar information, event date, presentation assets
   * If you would like guidance from DMP around certain items, please make a comment within the issue.
* Assign the issue to:
   * Leslie Stinson for Field, Microcampaigns, ABM, and Acceleration.
   * Matt Nguyen for Integrated Campaigns, non-campaign webcasts, and Corporate initiatives (such as All Remote).
* DMP will then create an issue for our digital agency PMG.
   * Only DMP can create issues within Digital Advertising (our project for PMG).
   * DMP will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
* DMP or MPM will create a design request issue if creative is needed for this campaign.
* DMP will approve copy & mockups if needed for this campaign.
   * PMG will generate copy & CTAs based on the landing page or content brief.
   * DMP will approve copy and mockups, unless approval from the campaign owner is requested.
* DMP will confirm launch within the issue.
* Reporting will be provided via Publisher, and PMG can also provide Publisher Reporting through Google Data Studio.

### DMP Request Timing
* Time required to get a campaign into market
   * Ideally, we need at least 3-4 weeks before the proposed launch date in order to plan strategy, forecast goals, and secure all creative assets.
* Turnaround time for mock-ups (if applicable)
   * Once creative & copy are generated, PMG can produce ad mockups for review upon request.
* Turnaround time to populate audiences (if applicable)
   * If the campaign is targeting a named account list, it can take 24-48 hours to populate within a platform.
* Turnaround time for Design
   * Depending on the request size, this could take at least 5 business days. A request for 2 LinkedIn images will take much less time than a request for a fully integrated campaign that requires multiple display & paid social sizes.
* Timing required for publisher engagements (sponsorships)
   * This will depend primarily on the publisher response time. Once PMG is connected with the publisher contact, 
* Expectations for reporting
   * Current DataStudio report(s) - request from DMP



## LinkedIn InMail Campaigns

LinkedIn InMail is an effective way to reach people at specific companies, industries, and more. They work best for inviting people to events and webinars if you do not have a specific set of people in mind (unlike a Marketo campaign). They can also be used to re-engage prospect accounts with upcoming events, new content assets, or by simply opening the conversation. If you are requesting InMail, create an issue in the general [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request).

### SDR Collaboration

InMail campaigns must have a designated sender in order to send messages on behalf of GitLab. The InMail ads displayed in a member’s inbox will appear to be sent from the designated sender, which allows for increased personalization and message relevance to targeted members.

In order for our digital agency PMG to set up sender permissions, they’ll need to send requests to those whom we want to grant sender permissions, which are typically SDRs. Since sender requests can only be sent to 1-degree connections, a member of the PMG team will first send a LinkedIn connection request, then the sender request once the connection is approved. When the SDR receives a request to be a sender on a LinkedIn Sponsored Messaging campaign, they can approve or reject this request by following the link provided in the email. The SDR can also [use this link](https://www.linkedin.com/ad/accounts?destination=sponsored-inmail-sender-permissions) to manage their sender permissions and see whether they have sender access for a campaign. Once the SDR has accepted their sender permissions to be added as a sender on the account, they will need to notify the DMP by confirming within the campaign issue.

### Copy

Although Digital Marketing can generate copy, we highly recommend that the sender or another member of the sales or field team generate the copy to be used in messages since they have more insight & context around targeted accounts. When creating an InMail campaign, the [InMail Ad Copy Template doc](https://docs.google.com/document/d/1KYSZJ5ALU2RvXz4CkqvxPnL7WmLen6XwU9D1qKH-Utw/edit) (included in the [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request)) will be cloned for your specific campaign and used as a collaborative space for the SDR to generate messaging and other campaign members to edit where necessary. Please note that due to the high volume of InMail messages within a given campaign, LinkedIn restricts replies from the target audience to the sender. If the SDR prefers replies, they can include a link to their Calendly or email address in the message.

InMail Copy Best Practices:
* Customize the greeting with the member’s name 
* Refer to their job title  
* Try using the word “you"
* The copy is fewer than 1,000 characters
* The CTA is clear
* The landing page is optimized for mobile

Best performing subject lines often use some of the following keywords:
* Thanks 
* Exclusive invitation  
* Connect 
* Job opportunities  
* Join us  

Additional ways that we can test with message tone of voice:
* Genuine
* The Helpful Advisor
* VIP Invitation - sending personalized invites to 'exclusive' events
* The Cliffhanger

## UTMs for URL tagging and tracking
All URLs that are promoted on external sites and through email must use UTM URL tagging to increase the data cleanliness in Google Analytics and ensure marketing campaigns are correctly attributed. 

We don't use UTMs for internal links. UTM data sets attribution for visitors, so if we use UTMs on internal links it resets everything when the clicked URL loads. This breaks reporting for paid advertising and organic visitors.

You can access our internal [URL tagging tool in Google Sheets](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0). You will also find details in this spreadsheet on what "Campaign Medium" to use for each URL. If you need a new campaign medium, please check with the Digital Marketing Programs team as new mediums will not automatically be attributed correctly.

If you are not sure if a link needs a UTM, please speak with the marketer who is managing your campaign to ensure you are not interrupting the reporting structure they have in place.

UTM construction best practices:
- lowercase only, not camelcase
- alphanumeric characters only
- no spaces
- **Campaign Medium** covers general buckets like `paidsearch`, `social`, or `sponsorship` 
- **Campaign Source** names where the link lives. Examples include `ebook`, `twitter`, or `qrcode`
- **Campaign Name** describes a specific campaign. Try to add additional context like `reinvent`, `forrester`, and `bugbounty`
- **Campaign Content** differentiates ad types.
- **Campaign Term** identifies keywords used in a campaign


## Opt-Out of Seeing GitLab Digital Ads

We run digital ads on the following channels:

* Google (paid search and display)
* Facebook
* LinkedIn
* Twitter
* Demandbase (paid display)

Because everyone at GitLab works remotely, it makes it difficult to restrict ads from being shown to GitLabbers. In a brick and mortar environment, we can easily block IP addresses to accomplish this. Because everyone at GitLab has a different IP address, and even dynamic IP addresses, there is no way to implement an exclusion rule that would block all GitLab ads to GitLabbers. 

If you would not like to see GitLab ads, you are able to opt-out of ads as you see them. Below is the process to remove GitLab ads. Please note that if you use both your personal and work accounts on your devices, you will need to exclude from both your personal and work accounts. 

**Google Paid Search**: When you see a GitLab text ad in Google search engine results
1. On the ad, click on the arrow next to the URL
2. Click on Why this ad? 
3. On the toggle next to “Show ads from gitlab.com”, toggle off
4. Close out of box

**Google Display**: When you see a GitLab ad on a third-party website
1. Click on the blue arrow icon at the top right-hand corner for the ad 
2. Select “Stop seeing this ad” when the option appears
3. Make a selection on why you do not want to see the ad anymore
4. Done

**Facebook**: When you see a GitLab ad on Facebook
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When, menu appears, click on “Why am I seeing this ad?”
3. In the next pop-up box, under “What You Can Do”, click the “Hide” button next to the option that says “Hide all ads from this advertiser”

**LinkedIn**: When you see a GitLab ad on LinkedIn
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When option box appears, click on “Report this ad, I don’t want to see this ad in my feed”
3. In the pop-up box that appears next, select an option
4. Click the Submit button on the next page

**Twitter**: When you see a GitLab ad on Twitter
1. On the upper right-hand corner of the ad, click the arrow
2. When option box appears, click on “I don’t like this ad”
* Note that this does not necessarily block all ads, just that specific ad. You would need to do this on all the ads you see from GitLab
* You could block @gitlab in your profile to no longer see ads. However you block all ability to engage with the @gitlab Twitter accounts as well (seeing Tweets, Retweeting, mentioning, etc). 


